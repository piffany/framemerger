import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;


public class ImageManip {

	public static BufferedImage joinBufferedImage(BufferedImage img1, BufferedImage img2,
			int offset, char orientation) {
		BufferedImage[] images = {img1, img2};
		return joinBufferedImage(images, offset, orientation);
	}

	public static BufferedImage joinBufferedImage(BufferedImage[] images,
			int offset, char orientation) {
		// orientation = 'h' or 'v'
		int wid = (orientation=='h') ? getTotalWidth(images, offset) : getMaxWidth(images);
		int height = (orientation=='v') ? getTotalHeight(images, offset) : getMaxHeight(images);
		//create a new buffer and draw two image into the new image
		BufferedImage newImage = new BufferedImage(wid, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = newImage.createGraphics();
		Color oldColor = g2.getColor();
		//fill background
		g2.setPaint(Color.WHITE);
		g2.fillRect(0, 0, wid, height);
		//draw image
		g2.setColor(oldColor);
		int x = 0, y = 0;
		for(int i=0; i<images.length; i++) {
			g2.drawImage(images[i], null, x, y);
			if(orientation == 'h') x += images[i].getWidth() + offset;
			else y += images[i].getHeight() + offset;
		}
		g2.dispose();
		return newImage;
	}

	private static int getTotalWidth(BufferedImage[] images, int offset) {
		int total = 0;
		for(int i=0; i<images.length; i++) {
			total += images[i].getWidth();
			if(i>0) total += offset;
		}
		return total;
	}

	private static int getTotalHeight(BufferedImage[] images, int offset) {
		int total = 0;
		for(int i=0; i<images.length; i++) {
			total += images[i].getHeight();
			if(i>0) total += offset;
		}
		return total;
	}

	private static int getMaxWidth(BufferedImage[] images) {
		int max = 0;
		for(int i=0; i<images.length; i++) {
			max = Math.max(max, images[i].getWidth());
		}
		return max;
	}

	private static int getMaxHeight(BufferedImage[] images) {
		int max = 0;
		for(int i=0; i<images.length; i++) {
			max = Math.max(max, images[i].getHeight());
		}
		return max;
	}

	public static BufferedImage addLabel(BufferedImage img, String label, int fontSize) {
		int w = img.getWidth();
		int h = img.getHeight();
		BufferedImage newImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = newImage.createGraphics();
		g2.drawImage(img, null, 0, 0);
		g2.setColor(Color.red);
		g2.setFont(new Font("TimesRoman", Font.PLAIN, fontSize));
		g2.drawString(label, fontSize, fontSize);
		g2.dispose();
		return newImage;
	}

	public static BufferedImage mergeImageMatrix(BufferedImage[][] images) {
		int nRows = images.length;
		BufferedImage[] rowImages = new BufferedImage[nRows];
		int offset = 0;
		for(int r=0; r<nRows; r++) {
			BufferedImage rowImage = joinBufferedImage(images[r], offset, 'h');
			rowImages[r] = rowImage;
		}
		BufferedImage matrixImage = joinBufferedImage(rowImages, offset, 'v');
		return matrixImage;
	}
}
