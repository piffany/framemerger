import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


@SuppressWarnings("serial")
public class FMGui
extends JFrame
implements ActionListener, ChangeListener {
	int nRows = 1;
	int nCols = 1;
	MyPanel[][] panels;
	JPanel inputPanel;
	OutputPanel outputPanel;
	int labelFontSize = 45;

	public FMGui(int nRows, int nCols) {
		super("Frame Merger");
		setSize(900,600);
		this.nRows = nRows;
		this.nCols = nCols;
		panels = new MyPanel[nRows][nCols];
		init();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void exit() { 
		setVisible(false);
		dispose(); 
		System.exit(0); 
	}

	private void init() {
		BorderLayout borderLayout = new BorderLayout();
		setLayout(borderLayout);

		inputPanel = new JPanel();
		GridLayout gridLayout = new GridLayout(nRows,nCols);
		inputPanel.setLayout(gridLayout);

		for(int r=0; r<nRows; r++) {
			for(int c=0; c<nCols; c++) {
				addPanel(r,c);
			}
		}
		outputPanel = new OutputPanel(this);
		
		add(inputPanel, BorderLayout.CENTER);
		add(outputPanel, BorderLayout.SOUTH);
	}

	private void addPanel(int r, int c) {
		MyPanel panel = new MyPanel(this);
		panels[r][c] = panel;
		inputPanel.add(panel);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		for(int r=0; r<nRows; r++) {
			for(int c=0; c<nCols; c++) {
				boolean performed = panels[r][c].actionPerformed(e);
				if(performed) return;
			}
		}
		outputPanel.actionPerformed(e);
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		outputPanel.stateChanged(e);
	}

	private boolean isDataValid(int[] matrixSize, String[][] inputDirs,
			String outputDir, String outputFormat, int min, int max) {
		if(matrixSize[0]==0 || matrixSize[1]==0) {
			System.out.println("Need to specify at least one input folder");
			JOptionPane.showMessageDialog(this,
					"Need to specify at least one input folder.",
					"Input error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		for(int r=0; r<matrixSize[0]; r++) {
			for(int c=0; c<matrixSize[1]; c++) {
				if(panels[r][c].isSelected() &&
						!folderExists(inputDirs[r][c])) {
					JOptionPane.showMessageDialog(this,
							"The following input file directory does not exist: \n" +
									inputDirs[r][c],
									"Input error",
									JOptionPane.ERROR_MESSAGE);
					return false;
				}
			}
		}
		if(!outputPanel.isSelected()) {
			JOptionPane.showMessageDialog(this,
					"Must select an output file directory.",
					"Output error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if(!folderExists(outputDir)) {
			JOptionPane.showMessageDialog(this,
					"The output file directory does not exist: \n" +
							outputDir,
							"Output error",
							JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if(!(min >= 0 && min <= max)) {
			JOptionPane.showMessageDialog(this,
					"The file range must satisfy 0 <= min <= max.",
					"File range error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		for(int r=0; r<matrixSize[0]; r++) {
			for(int c=0; c<matrixSize[1]; c++) {
				if(panels[r][c].isSelected() &&
				   !allFilesExist(inputDirs[r][c], outputFormat, min, max)) {
					JOptionPane.showMessageDialog(this,
							"The following input file directory does not contain files of the specified format: \n" +
									inputDirs[r][c],
									"Input error",
									JOptionPane.ERROR_MESSAGE);
					return false;
				}
			}
		}
		if(anyFilesExist(outputDir, outputFormat, min, max)) {
			Object[] options = {"Yes", "No"};
			int n = JOptionPane.showOptionDialog(this,
					"Some files in the output file directory will be overwritten.\n"
							+ "Are you sure you want to continue?",
							"Overwrite warning",
							JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE,
							null,
							options,
							options[1]);
			System.out.println("n = " + n);
			// 0 = yes, 1 = no, -1 = dialog closed
			if (n!=0) return false;
		}
		return true;
	}

	public void generate(String outputDir, String outputFormat, int min, int max) {
		int[] matrixSize = getInputImageMatrixSize();
		String[][] inputDirs = getInputImageData();
		if(!isDataValid(matrixSize, inputDirs, outputDir, outputFormat, min, max)) return;
		for(int index = min; index <= max; index++) {
			try {
				BufferedImage[][] inputs = new BufferedImage[matrixSize[0]][matrixSize[1]];
				// get largest image size to use for padding
				int[] maxSize = {0,0};
				for(int r=0; r<matrixSize[0]; r++) {
					for(int c=0; c<matrixSize[1]; c++) {
						if(panels[r][c].isSelected()) {
							BufferedImage im = readInputImage(inputDirs[r][c], outputFormat, index);
							maxSize[0] = Math.max(maxSize[0], im.getWidth());
							maxSize[1] = Math.max(maxSize[1], im.getHeight());
						}
					}
				}
				for(int r=0; r<matrixSize[0]; r++) {
					for(int c=0; c<matrixSize[1]; c++) {
						BufferedImage im = panels[r][c].isSelected() ? 
								readInputImage(inputDirs[r][c], outputFormat, index) :
								new BufferedImage(maxSize[0],maxSize[1], BufferedImage.TYPE_INT_ARGB);
						String label = panels[r][c].getAnnotation();//String.format(outputFormat, index);
						im = ImageManip.addLabel(im, label, labelFontSize);
						inputs[r][c] = im;
					}
				}
				BufferedImage output = ImageManip.mergeImageMatrix(inputs);
				writeOutputImage(output, outputDir, outputFormat, index);
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private boolean allFilesExist(String dir, String format, int min, int max) {
		for(int i=min; i<=max; i++) {
			String imName = String.format(format, i);
			File f = new File(dir+"\\"+imName);
			try {
				ImageIO.read(f);
			}
			catch (IOException e) {
				return false;
			}
		}
		return true;
	}

	private boolean anyFilesExist(String dir, String format, int min, int max) {
		for(int i=min; i<=max; i++) {
			String imName = String.format(format, i);
			File f = new File(dir+"\\"+imName);
			try {
				ImageIO.read(f);
				return true;
			}
			catch (IOException e) {
			}
		}
		return false;
	}

	private boolean folderExists(String dir) {
		File f = new File(dir);
		return f.isDirectory();
	}

	private BufferedImage readInputImage(String dir, String format, int index) throws IOException {
		String imName = String.format(format, index);
		BufferedImage image = ImageIO.read(new File(dir+"\\"+imName));
		return image;
	}

	private void writeOutputImage(BufferedImage image, String dir, String format, int index) throws IOException {
		String imName = String.format(format, index);
		boolean success = ImageIO.write(image, "png", new File(dir+"\\"+imName));
		System.out.println((success ? "Success: " : "Failure: ") + imName);
	}

	private String[][] getInputImageData() {
		int[] matrixSize = getInputImageMatrixSize();
		String[][] inputDirs = new String[matrixSize[0]][matrixSize[1]];
		for(int r=0; r<matrixSize[0]; r++) {
			for(int c=0; c<matrixSize[1]; c++) {
				if(panels[r][c].isSelected()) {
					inputDirs[r][c] = panels[r][c].getDirectory();
				}
				else {
					inputDirs[r][c] = "";
				}
			}
		}
		return inputDirs;
	}

	public int[] getInputImageMatrixSize() {
		int[] matrixSize = {0,0};
		for(int r=0; r<nRows; r++) {
			for(int c=0; c<nCols; c++) {
				if(panels[r][c].isSelected()) {
					matrixSize[0] = Math.max(matrixSize[0], r+1);
					matrixSize[1] = Math.max(matrixSize[1], c+1);
				}
			}
		}
		System.out.println("matrixSize: " + matrixSize[0] + ", " + matrixSize[1]);
		return matrixSize;
	}
}
