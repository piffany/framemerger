public class Main {

	public static FMGui gui;
	
	public static void main(String [] args) {
		
		gui = new FMGui(4,4);
		gui.setVisible(true);
		/*
		String rootDir = "C:\\Users\\Piffany\\Documents\\Research\\Postdoc\\matlab-smoke-simulation\\ADMM_results\\blur";
		String dir1 = "sqSpiral2_blur_64x64_dt1_thick_x2_filter_10_rho_20_alpha_1",
			   dir2 = "sqSpiral2_blur_64x64_dt1_thick_x2_filter_10_rho_20_alpha_10",
			   dir3 = "sqSpiral2_blur_64x64_dt1_thick_x2_filter_10_rho_20_alpha_50",
			   dir4 = "sqSpiral2_blur_64x64_dt1_thick_x2_filter_10_rho_20_alpha_100";
		dir1 = rootDir + "\\" + dir1;
		dir2 = rootDir + "\\" + dir2;
		dir3 = rootDir + "\\" + dir3;
		dir4 = rootDir + "\\" + dir4;
		String dirOut = "C:\\Users\\Piffany\\Documents\\Research\\Postdoc\\matlab-smoke-simulation\\ADMM_results\\blur\\sqSpiral2_blur_64x64_dt1_thick_x2_filter_10_rho_20_alpha_var";
		String label1 = "alpha_1",
			   label2 = "alpha_10",
			   label3 = "alpha_50",
			   label4 = "alpha_100";
		String prefix = "im_";
		String suffix = "png";
		int offset = 0;
		int fontSize = 45;
		int maxIndex = 350;
		int numDigits = (int) Math.floor(Math.log10(maxIndex))+1;
		for(int index = 1; index <= maxIndex; index ++) {
			try {
				String imCount = String.format("%0"+numDigits+"d", index);
				String imName = prefix + imCount + "." + suffix;
	            BufferedImage img1 = ImageIO.read(new File(dir1+"\\"+imName));
	            BufferedImage img2 = ImageIO.read(new File(dir2+"\\"+imName));
	            BufferedImage img3 = ImageIO.read(new File(dir3+"\\"+imName));
	            BufferedImage img4 = ImageIO.read(new File(dir4+"\\"+imName));
	            img1 = ImageManip.addLabel(img1, label1, fontSize);
	            img2 = ImageManip.addLabel(img2, label2, fontSize);
	            img3 = ImageManip.addLabel(img3, label3, fontSize);
	            img4 = ImageManip.addLabel(img4, label4, fontSize);
	            
	            BufferedImage img12 = ImageManip.joinBufferedImage(img1,img2,offset,'h');
	            BufferedImage img34 = ImageManip.joinBufferedImage(img3,img4,offset,'h');
	            BufferedImage joinedImg = ImageManip.joinBufferedImage(img12,img34,offset,'v');
	            boolean success = ImageIO.write(joinedImg, "png", new File(dirOut+"\\"+imName));
	            System.out.println("saved success? "+success + " (" + index + ")");
	        } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
		}
		*/
	}
}
