import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;

@SuppressWarnings("serial")
public class OutputPanel extends JPanel {
	
	private JButton sourceButton;
	private String dir;
	private JTextArea dirTextArea;
	private JTextArea formatTextArea;
	private boolean selected = false;
	private static Color bgGray = Color.lightGray;//new Color(238,238,238);
	private static Color lightGreen = new Color(180,255,180);
	private JButton generateButton;
	private SpinnerModel modelMin, modelMax;
	private JSpinner spinnerMin, spinnerMax;
	private JPanel innerPanel, row1, row2, row3, row4, row4a, row4b, row5;
	private FMGui gui;
	
	public OutputPanel(FMGui gui) {
		
		this.gui = gui;
		
		setSelected(false);
		setOpaque(true);
		setBorder(BorderFactory.createLineBorder(Color.black));
		
		BoxLayout boxLayout = new BoxLayout(this, BoxLayout.X_AXIS);
		setLayout(boxLayout);
		
		sourceButton = new JButton("Open...");
		sourceButton.addActionListener(gui);
		
		row1 = new JPanel();
		row1.setLayout(new BoxLayout(row1,BoxLayout.X_AXIS));
		row1.add(Box.createHorizontalGlue());
		row1.add(sourceButton);
		row1.add(Box.createHorizontalGlue());
		
		JLabel fileDirLabel = new JLabel("Output File Directory:");

		setSource("");
		dirTextArea = new JTextArea(2,15);
		dirTextArea.setEditable(false);
		dirTextArea.setLineWrap(true);
		dirTextArea.setVisible(true);
		dirTextArea.setOpaque(true);
		dirTextArea.setWrapStyleWord(false);
		dirTextArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 10));
		JScrollPane scroll1 = new JScrollPane (dirTextArea);
		row2 = new JPanel();
		row2.setLayout(new BoxLayout(row2, BoxLayout.Y_AXIS));
		row2.add(Box.createVerticalGlue());
		row2.add(fileDirLabel);
		row2.add(scroll1);
		row2.add(Box.createVerticalGlue());
	    
		JLabel formatDirLabel = new JLabel("File Format (e.g. \"im_%03d.png\"):");
		formatTextArea = new JTextArea(2,15);
		formatTextArea.setText("im_%03d.png");
		formatTextArea.setEditable(true);
		formatTextArea.setLineWrap(true);
		formatTextArea.setVisible(true);
		formatTextArea.setOpaque(true);
		formatTextArea.setWrapStyleWord(false);
		formatTextArea.setBackground(Color.white);
		formatTextArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 10));
		JScrollPane scroll2 = new JScrollPane (formatTextArea);

		row3 = new JPanel();
		row3.setLayout(new BoxLayout(row3, BoxLayout.Y_AXIS));
		row3.add(Box.createVerticalGlue());
		row3.add(formatDirLabel);
		row3.add(scroll2);
		row3.add(Box.createVerticalGlue());
	    
		JLabel minLabel = new JLabel("Min:");
		add(minLabel);
		
		modelMin = new SpinnerNumberModel(1, 1, 99999, 1);
		spinnerMin = new JSpinner(modelMin);
		spinnerMin.addChangeListener(gui);
		add(spinnerMin);
		
		JLabel maxLabel = new JLabel("Max:");
		add(maxLabel);
	    
		modelMax = new SpinnerNumberModel(1, 1, 99999, 1);
		spinnerMax = new JSpinner(modelMax);
		spinnerMax.addChangeListener(gui);
		add(spinnerMax);

		row4 = new JPanel();
		row4.setLayout(new BoxLayout(row4, BoxLayout.Y_AXIS));
		row4a = new JPanel();
		row4a.setLayout(new BoxLayout(row4a, BoxLayout.X_AXIS));
		row4a.add(Box.createHorizontalGlue());
		row4a.add(minLabel);
		row4a.add(Box.createHorizontalGlue());
		row4a.add(spinnerMin);
		
		row4b = new JPanel();
		row4b.setLayout(new BoxLayout(row4b, BoxLayout.X_AXIS));
		row4b.add(Box.createHorizontalGlue());
		row4b.add(maxLabel);
		row4b.add(Box.createHorizontalGlue());
		row4b.add(spinnerMax);
		
		row4.add(Box.createVerticalGlue());
		row4.add(row4a);
		row4.add(Box.createVerticalGlue());
		row4.add(row4b);
		row4.add(Box.createVerticalGlue());

		generateButton = new JButton("generate");
	    generateButton.addActionListener(gui);
	    row5 = new JPanel();
		row5.setLayout(new BoxLayout(row5,BoxLayout.X_AXIS));
		row5.add(Box.createHorizontalGlue());
		row5.add(generateButton);
		row5.add(Box.createHorizontalGlue());
	    
	    innerPanel = new JPanel();
	    BoxLayout boxLayout2 = new BoxLayout(innerPanel, BoxLayout.X_AXIS);
	    innerPanel.setLayout(boxLayout2);
	    
	    add(Box.createVerticalGlue());
	    add(innerPanel);
	    add(Box.createVerticalGlue());
	    
	    innerPanel.add(Box.createHorizontalGlue());
	    innerPanel.add(row1);
	    innerPanel.add(Box.createHorizontalGlue());
	    innerPanel.add(row2);
	    innerPanel.add(Box.createHorizontalGlue());
	    innerPanel.add(row3);
	    innerPanel.add(Box.createHorizontalGlue());
	    innerPanel.add(row4);
	    innerPanel.add(Box.createHorizontalGlue());
	    innerPanel.add(row5);
	    innerPanel.add(Box.createVerticalGlue());
	
		refresh();
		this.setVisible(true);

	}
	
	public void setPanelBackgrounds(Color c) {
		super.setBackground(c);
		innerPanel.setBackground(c);
		row1.setBackground(c);
		row2.setBackground(c);
		row3.setBackground(c);
		row4.setBackground(c);
		row4a.setBackground(c);
		row4b.setBackground(c);
		row5.setBackground(c);
	}
	
	public void refresh() {
		setPanelBackgrounds(selected? lightGreen : bgGray);
		dirTextArea.setBackground(
				dirTextArea.isEditable() ? Color.white : bgGray);
		dirTextArea.setText(dir);
	}

	public JButton getSourceButton() {
		return sourceButton;
	}

	public void setSourceButton(JButton sourceButton) {
		this.sourceButton = sourceButton;
	}

	public String getSource() {
		return dir;
	}

	public void setSource(String source) {
		this.dir = source;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean actionPerformed(ActionEvent e) {
		if(e.getSource() == sourceButton) {
			sourceButtonPressed();
			return true;
		}
		else if(e.getSource() == generateButton) {
			generateButtonPressed();
			return true;
		}
		return false;
	}
	
	private void generateButtonPressed() {
		int min = (int) modelMin.getValue();
		int max = (int) modelMax.getValue();
		String outputDir = dirTextArea.getText();
		String outputFormat = formatTextArea.getText();
		gui.generate(outputDir, outputFormat, min, max);
	}

	private void sourceButtonPressed() {
		JFileChooser chooser = new JFileChooser();
	    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    int returnVal = chooser.showOpenDialog(this);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	    	this.dir = chooser.getSelectedFile().getAbsolutePath();
	    	selected = true;
	    }
		refresh();
	}

	public boolean stateChanged(ChangeEvent e) {
		if(e.getSource() == spinnerMin) {
			spinnerMinChanged();
			return true;
		}
		else if(e.getSource() == spinnerMax) {
			spinnerMaxChanged();
			return true;
		}
		else return false;
	}

	private void spinnerMinChanged() {
		modelMin.setValue((int) spinnerMin.getValue());
		refresh();
	}
	
	private void spinnerMaxChanged() {
		modelMax.setValue((int) spinnerMax.getValue());
		refresh();
	}
}
