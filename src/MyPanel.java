import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class MyPanel extends JPanel {
	
	private JButton sourceButton;
	private JButton cancelButton;
	private String fileDir;
	private JTextArea fileDirTextArea;
	private JTextArea annotationTextArea;
	private boolean selected = false;
	private static Color bgGray = new Color(220,220,220);
	private static Color lightGreen = new Color(180,255,180);
	private JPanel innerPanel, row1, row2, row3;
	
	public MyPanel(FMGui gui) {
		
		setSelected(false);
		setOpaque(true);
		setBorder(BorderFactory.createLineBorder(Color.black));
		
		BoxLayout boxLayout = new BoxLayout(this, BoxLayout.X_AXIS);
		setLayout(boxLayout);
		
		sourceButton = new JButton("Open...");
		sourceButton.addActionListener(gui);
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(gui);
		
		row1 = new JPanel();
		row1.setLayout(new BoxLayout(row1, BoxLayout.X_AXIS));
		row1.add(Box.createHorizontalGlue());
		row1.add(sourceButton);
		row1.add(Box.createHorizontalGlue());
		row1.add(cancelButton);
		row1.add(Box.createHorizontalGlue());
		
		setSource("");

		JLabel fileDirLabel = new JLabel("Input File Directory:");
		fileDirTextArea = new JTextArea(3,5);
		fileDirTextArea.setEditable(false);
		fileDirTextArea.setLineWrap(true);
		fileDirTextArea.setVisible(true);
		fileDirTextArea.setOpaque(true);
		fileDirTextArea.setWrapStyleWord(false);
		fileDirTextArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 10));
		JScrollPane scroll1 = new JScrollPane (fileDirTextArea);
		row2 = new JPanel();
		row2.setLayout(new BoxLayout(row2, BoxLayout.Y_AXIS));
		row2.add(fileDirLabel);
		row2.add(scroll1);
		
		JLabel annotationLabel = new JLabel("Annotation:");
		annotationTextArea = new JTextArea(1,5);
		annotationTextArea.setEditable(true);
		annotationTextArea.setLineWrap(true);
		annotationTextArea.setVisible(true);
		annotationTextArea.setOpaque(true);
		annotationTextArea.setWrapStyleWord(false);
		annotationTextArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 10));
		JScrollPane scroll2 = new JScrollPane (annotationTextArea);
		row3 = new JPanel();
		row3.setLayout(new BoxLayout(row3, BoxLayout.Y_AXIS));
		row3.add(annotationLabel);
		row3.add(scroll2);
	    
	    innerPanel = new JPanel();
	    BoxLayout boxLayout2 = new BoxLayout(innerPanel, BoxLayout.Y_AXIS);
	    innerPanel.setLayout(boxLayout2);
	    
	    add(Box.createHorizontalGlue());
	    add(innerPanel);
	    add(Box.createHorizontalGlue());
	    
	    innerPanel.add(Box.createVerticalGlue());
	    innerPanel.add(row1);
	    innerPanel.add(Box.createVerticalGlue());
	    innerPanel.add(row2);
	    innerPanel.add(Box.createVerticalGlue());
	    innerPanel.add(row3);
	    innerPanel.add(Box.createVerticalGlue());
	
		refresh();
		this.setVisible(true);

	}
	
	public void setPanelBackgrounds(Color c) {
		super.setBackground(c);
		innerPanel.setBackground(c);
		row1.setBackground(c);
		row2.setBackground(c);
		row3.setBackground(c);
	}
	
	public void refresh() {
		setPanelBackgrounds(selected? lightGreen : bgGray);
		fileDirTextArea.setBackground(
				fileDirTextArea.isEditable() ? Color.white : bgGray);
		fileDirTextArea.setText(fileDir);
	}

	public JButton getSourceButton() {
		return sourceButton;
	}

	public void setSourceButton(JButton sourceButton) {
		this.sourceButton = sourceButton;
	}

	public JButton getCancelButton() {
		return cancelButton;
	}

	public void setCancelButton(JButton cancelButton) {
		this.cancelButton = cancelButton;
	}

	public String getSource() {
		return fileDir;
	}

	public void setSource(String source) {
		this.fileDir = source;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean actionPerformed(ActionEvent e) {
		if(e.getSource() == sourceButton) {
			sourceButtonPressed();
			return true;
		}
		else if(e.getSource() == cancelButton) {
			cancelButtonPressed();
			return true;
		}
		return false;
	}

	private void cancelButtonPressed() {
		selected = false;
		fileDir = "";
		refresh();
	}

	private void sourceButtonPressed() {
		JFileChooser chooser = new JFileChooser();
	    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    int returnVal = chooser.showOpenDialog(this);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	    	this.fileDir = chooser.getSelectedFile().getAbsolutePath();
	    	selected = true;
	    }
		refresh();
	}
	
	public String getDirectory() {
		return fileDir;
	}
	
	public String getAnnotation() {
		return annotationTextArea.getText();
	}
}
